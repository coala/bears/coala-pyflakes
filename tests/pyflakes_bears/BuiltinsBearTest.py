from queue import Queue

from coalib.results.Result import Result
from coalib.settings.Section import Section
from coalib.settings.Setting import Setting
from coalib.testing.LocalBearTestHelper import LocalBearTestHelper

from pyflakes_bears.BuiltinsBear import BuiltinsBear


class NoFutureImportTest(LocalBearTestHelper):

    def setUp(self):
        self.section = Section('builtins')
        self.uut = BuiltinsBear(self.section, Queue())
        self.filename = 'BuiltinsBear'

    def test_valid(self):
        self.check_validity(self.uut, ['__file__ = 10'])
        self.check_validity(self.uut, ['def foo(range=range): pass'])
        self.check_validity(self.uut, ['class Foo: \n',
                                       '    def range(self): pass \n'])
        self.check_validity(self.uut, ['try: \n',
                                       '    max \n',
                                       'except NameError: \n',
                                       '    def max(a, b): pass'])

    def test_strict(self):
        self.section.append(Setting('strict', True))

        self.test_valid()

        self.check_results(
            self.uut,
            ['def max(): pass'],
            [Result.from_values('BuiltinsBear',
                                'redefinition of builtin \'max\'',
                                file=self.filename,
                                line=1,
                                column=1)],
            filename=self.filename)

        self.check_results(
            self.uut,
            ['class range: pass'],
            [Result.from_values('BuiltinsBear',
                                'redefinition of builtin \'range\'',
                                file=self.filename,
                                line=1,
                                column=1)],
            filename=self.filename)

        self.check_results(
            self.uut,
            ['import min'],
            [Result.from_values('BuiltinsBear',
                                'redefinition of builtin \'min\'',
                                file=self.filename,
                                line=1,
                                column=1)],
            filename=self.filename)

    def test_loose(self):
        self.section.append(Setting('strict', False))

        self.test_valid()
        self.check_validity(self.uut, ['def max(): pass'])
        self.check_validity(self.uut, ['class max: pass'])
        self.check_validity(self.uut, ['import max'])

        file_text = ['max(1) \n',
                     'def max(): pass \n']
        self.check_results(
            self.uut,
            file_text,
            [Result.from_values('BuiltinsBear',
                                'builtin \'max\' redefined was '
                                'recently used at 1',
                                file=self.filename,
                                line=2,
                                column=1)],
            filename=self.filename)

        file_text = ['def max(): pass \n',
                     'def max(): pass \n']
        self.check_results(
            self.uut,
            file_text,
            [Result.from_values('BuiltinsBear',
                                'redefinition of unused '
                                'builtin \'max\' from line 1',
                                file=self.filename,
                                line=2,
                                column=1)],
            filename=self.filename)
